package co.com.zemoga.portfolio.domain.services.queries;

import co.com.zemoga.portfolio.domain.services.IPortfolioQueryService;
import co.com.zemoga.portfolio.infrastructure.registries.Portfolio;
import co.com.zemoga.portfolio.infrastructure.repositories.PortfolioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class PortfolioQueryService implements IPortfolioQueryService {

    @Autowired
    private PortfolioRepository repository;

    @Override
    public Portfolio getPortfolio(Integer id) {
        return repository.findById(id).orElseGet(() -> new Portfolio());
    }

    @Override
    public Portfolio getPortfolio(String user) {
        List<Portfolio> list = repository.findByUser(user).stream().collect(Collectors.toList());
        return list.stream().findFirst().orElseGet(() -> new Portfolio());
    }
}
