package co.com.zemoga.portfolio.infrastructure.repositories;

import co.com.zemoga.portfolio.infrastructure.registries.Portfolio;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PortfolioRepository extends JpaRepository<Portfolio, Integer> {

    List<Portfolio> findByUser(String user);
}
