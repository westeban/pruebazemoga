package co.com.zemoga.portfolio.domain.services;

import co.com.zemoga.portfolio.infrastructure.registries.Portfolio;

public interface IPortfolioCommandService {

    public Portfolio updatePortfolio(Portfolio portfolio);

}
