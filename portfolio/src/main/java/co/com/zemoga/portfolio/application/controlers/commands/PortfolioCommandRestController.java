package co.com.zemoga.portfolio.application.controlers.commands;

import co.com.zemoga.portfolio.domain.services.IPortfolioCommandService;
import co.com.zemoga.portfolio.infrastructure.registries.Portfolio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class PortfolioCommandRestController {

    @Autowired
    private IPortfolioCommandService portfolioCommandsService;

    @PutMapping(value = "/users")
    public Portfolio updatePortfolio(@RequestBody Portfolio portfolio) {
        portfolioCommandsService.updatePortfolio(portfolio);
        return portfolio;
    }


}
