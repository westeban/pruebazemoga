package co.com.zemoga.portfolio.application.controlers.queries;

import co.com.zemoga.portfolio.domain.services.IPortfolioQueryService;
import co.com.zemoga.portfolio.infrastructure.registries.Portfolio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class PortfolioQueryRestController {

    @Autowired
    private IPortfolioQueryService portfolioQueryService;

    @GetMapping(value = "/users/id/{userId}")
    public Portfolio getPortfolio(@PathVariable Integer userId) {
        return portfolioQueryService.getPortfolio(userId);
    }

    @GetMapping(value = "/users/{user}")
    public Portfolio getPortfolio(@PathVariable String user) {
        return portfolioQueryService.getPortfolio(user);
    }


}
