package co.com.zemoga.portfolio.infrastructure.registries;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "portfolio")
public class Portfolio {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "idportfolio")
    private Integer id;
    @Column(name = "description")
    private String description;
    @Column(name = "image_url")
    private String image;
    @Column(name = "twitter_user_name")
    private String user;
    @Column(name = "title")
    private String title;

    public Portfolio() {
    }

    public Portfolio(Integer id, String description, String image, String user, String title) {
        this.id = id;
        this.description = description;
        this.image = image;
        this.user = user;
        this.title = title;
    }

    public Portfolio(String description, String image, String user, String title) {
        this.description = description;
        this.image = image;
        this.user = user;
        this.title = title;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Portfolio portfolio = (Portfolio) o;
        return Objects.equals(id, portfolio.id) &&
                Objects.equals(description, portfolio.description) &&
                Objects.equals(image, portfolio.image) &&
                Objects.equals(user, portfolio.user) &&
                Objects.equals(title, portfolio.title);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, description, image, user, title);
    }
}
