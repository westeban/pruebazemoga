package co.com.zemoga.portfolio.domain.services.commands;

import co.com.zemoga.portfolio.domain.services.IPortfolioCommandService;
import co.com.zemoga.portfolio.infrastructure.registries.Portfolio;
import co.com.zemoga.portfolio.infrastructure.repositories.PortfolioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PortfolioCommandService implements IPortfolioCommandService {

    @Autowired
    private PortfolioRepository repository;

    @Override
    public Portfolio updatePortfolio(Portfolio portfolio) {
        return repository.save(portfolio);
    }
}
