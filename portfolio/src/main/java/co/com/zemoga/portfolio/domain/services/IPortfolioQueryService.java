package co.com.zemoga.portfolio.domain.services;

import co.com.zemoga.portfolio.infrastructure.registries.Portfolio;

public interface IPortfolioQueryService {

    Portfolio getPortfolio(Integer id);

    Portfolio getPortfolio(String user);

}
