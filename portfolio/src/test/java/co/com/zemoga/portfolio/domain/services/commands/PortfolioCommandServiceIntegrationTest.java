package co.com.zemoga.portfolio.domain.services.commands;

import co.com.zemoga.portfolio.infrastructure.registries.Portfolio;
import co.com.zemoga.portfolio.infrastructure.repositories.PortfolioRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
public class PortfolioCommandServiceIntegrationTest {

    @Mock
    PortfolioRepository repository;

    @InjectMocks
    PortfolioCommandService service;

    @Test
    public void whenUpdatePortfolio_thenReturnPortfolio() {
        Integer id = 102;
        Portfolio portfolio = new Portfolio(id, "description", "image", "user", "title");
        when(repository.save(portfolio)).thenReturn(portfolio);

        Portfolio result = service.updatePortfolio(portfolio);

        assertThat(result).isEqualToComparingFieldByField(portfolio);
        verify(repository, only()).save(portfolio);
    }
}
