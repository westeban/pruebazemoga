package co.com.zemoga.portfolio.domain.services.queries;

import co.com.zemoga.portfolio.infrastructure.registries.Portfolio;
import co.com.zemoga.portfolio.infrastructure.repositories.PortfolioRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
public class PortfolioQueryServiceTest {

    @Mock
    PortfolioRepository repository;

    @InjectMocks
    PortfolioQueryService service;

    @Test
    public void whenGetPortfolioById_thenReturnPortfolio() {
        Integer id = 102;
        Portfolio portfolio = new Portfolio(id, "description", "image", "user", "title");
        when(repository.findById(id)).thenReturn(Optional.of(portfolio));

        Portfolio result = service.getPortfolio(id);

        assertThat(result).isEqualToComparingFieldByField(portfolio);
        verify(repository, only()).findById(id);
    }

    @Test
    public void whenGetPortfolioById_thenReturnPortfolioEmpty() {
        Integer id = 102;
        Mockito.when(repository.findById(id)).thenReturn(Optional.empty());

        Portfolio result = service.getPortfolio(id);

        assertThat(result).isEqualToComparingFieldByField(new Portfolio());
        verify(repository, only()).findById(id);
    }

    @Test
    public void whenGetPortfolioByUser_thenReturnPortfolio() {
        String user = "user";
        Portfolio portfolio = new Portfolio(102, "description", "image", user, "title");
        List list = new ArrayList();
        list.add(portfolio);
        when(repository.findByUser(user)).thenReturn(list);

        Portfolio result = service.getPortfolio(user);

        assertThat(result).isEqualToComparingFieldByField(portfolio);
        verify(repository, only()).findByUser("user");
    }

    @Test
    public void whenGetPortfolioByUser_thenReturnPortfolioEmpty() {
        List list = new ArrayList();
        when(repository.findByUser("user")).thenReturn(list);

        Portfolio result = service.getPortfolio("user");

        assertThat(result).isEqualToComparingFieldByField(new Portfolio());
        verify(repository, only()).findByUser("user");
    }
}
