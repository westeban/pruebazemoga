package co.com.zemoga.portfolio.application.controllers.Command;

import co.com.zemoga.portfolio.PortfolioApplication;
import co.com.zemoga.portfolio.infrastructure.registries.Portfolio;
import co.com.zemoga.portfolio.infrastructure.repositories.PortfolioRepository;
import co.com.zemoga.portfolio.infrastructure.util.JsonUtil;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.fail;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = PortfolioApplication.class)
@AutoConfigureMockMvc
@AutoConfigureTestDatabase
public class PortfolioCommandRestControllerIntegrationTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private PortfolioRepository repository;

    @After
    public void resetDb() {
        repository.deleteAll();
    }

    @Test
    public void whenPutPortfolio_thenUpdateAndReturnPortfolio() {
        Portfolio portfolio = new Portfolio("description", "image", "user", "title");
        portfolio = repository.save(portfolio);
        repository.flush();

        try {
            portfolio.setDescription("Description change");
            mvc.perform(put("/api/users/")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(JsonUtil.toJson(portfolio)))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.id", is(portfolio.getId())))
                    .andExpect(jsonPath("$.description", is(portfolio.getDescription())))
                    .andExpect(jsonPath("$.image", is(portfolio.getImage())))
                    .andExpect(jsonPath("$.user", is(portfolio.getUser())))
                    .andExpect(jsonPath("$.title", is(portfolio.getTitle())));
            Portfolio portfolioResult = repository.findById(portfolio.getId()).orElseGet(() -> new Portfolio());
            assertThat(portfolioResult).isEqualToComparingFieldByField(portfolio);
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }
}
