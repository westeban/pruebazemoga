package co.com.zemoga.portfolio.infrastructure.repositories;

import co.com.zemoga.portfolio.infrastructure.registries.Portfolio;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class PortfolioRepositoryIntegrationTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private PortfolioRepository repository;

    @Test
    public void whenFindByUser_thenReturnPortfolio() {
        Portfolio portfolio = new Portfolio("description test", "image test", "userTest", "title test");
        entityManager.persist(portfolio);
        entityManager.flush();

        Portfolio found = repository.findByUser(portfolio.getUser()).get(0);

        assertThat(found.getDescription()).isEqualTo(portfolio.getDescription());
    }

    @Test
    public void whenSave_thenUpdateAndReturnPortfolio() {
        Portfolio portfolio = new Portfolio("description test", "image test", "userTest", "title test");
        entityManager.persist(portfolio);
        entityManager.flush();
        portfolio.setDescription("description test update");

        Portfolio found = repository.save(portfolio);

        assertThat(found.getDescription()).isEqualTo(portfolio.getDescription());
    }
}
