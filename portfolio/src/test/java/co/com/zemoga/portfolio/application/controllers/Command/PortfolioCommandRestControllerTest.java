package co.com.zemoga.portfolio.application.controllers.Command;

import co.com.zemoga.portfolio.application.controlers.commands.PortfolioCommandRestController;
import co.com.zemoga.portfolio.domain.services.IPortfolioCommandService;
import co.com.zemoga.portfolio.infrastructure.registries.Portfolio;
import co.com.zemoga.portfolio.infrastructure.util.JsonUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.fail;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.only;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(PortfolioCommandRestController.class)
public class PortfolioCommandRestControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private IPortfolioCommandService service;


    @Test
    public void whenPutPortfolio_thenUpdateAndReturnPortfolio() {
        Portfolio portfolio = new Portfolio(102, "description", "image", "user", "title");

        given(service.updatePortfolio(portfolio)).willReturn(portfolio);

        try {
            mvc.perform(put("/api/users/")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(JsonUtil.toJson(portfolio)))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.id", is(portfolio.getId())))
                    .andExpect(jsonPath("$.description", is(portfolio.getDescription())))
                    .andExpect(jsonPath("$.image", is(portfolio.getImage())))
                    .andExpect(jsonPath("$.user", is(portfolio.getUser())))
                    .andExpect(jsonPath("$.title", is(portfolio.getTitle())));
            verify(service, only()).updatePortfolio(portfolio);
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }

}
