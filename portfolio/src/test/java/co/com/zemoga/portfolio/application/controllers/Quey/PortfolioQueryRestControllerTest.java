package co.com.zemoga.portfolio.application.controllers.Quey;

import co.com.zemoga.portfolio.application.controlers.queries.PortfolioQueryRestController;
import co.com.zemoga.portfolio.domain.services.IPortfolioQueryService;
import co.com.zemoga.portfolio.infrastructure.registries.Portfolio;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.fail;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.only;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(PortfolioQueryRestController.class)
public class PortfolioQueryRestControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private IPortfolioQueryService service;

    @Test
    public void givenUserId_whenGetPortfolio_thenReturnPortfolio() {
        Integer id = 102;
        Portfolio portfolio = new Portfolio(id, "description", "image", "user", "title");

        given(service.getPortfolio(id)).willReturn(portfolio);

        try {
            mvc.perform(get("/api/users/id/"+id)
            .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.id", is(portfolio.getId())))
                    .andExpect(jsonPath("$.description", is(portfolio.getDescription())))
                    .andExpect(jsonPath("$.image", is(portfolio.getImage())))
                    .andExpect(jsonPath("$.user", is(portfolio.getUser())))
                    .andExpect(jsonPath("$.title", is(portfolio.getTitle())));
            verify(service, only()).getPortfolio(id);
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void givenUser_whenGetPortfolio_thenReturnPortfolio() {
        String user = "user";
        Portfolio portfolio = new Portfolio(102, "description", "image", user, "title");

        given(service.getPortfolio(user)).willReturn(portfolio);

        try {
            mvc.perform(get("/api/users/"+user)
            .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.id", is(portfolio.getId())))
                    .andExpect(jsonPath("$.description", is(portfolio.getDescription())))
                    .andExpect(jsonPath("$.image", is(portfolio.getImage())))
                    .andExpect(jsonPath("$.user", is(portfolio.getUser())))
                    .andExpect(jsonPath("$.title", is(portfolio.getTitle())));
            verify(service, only()).getPortfolio(user);
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }
}
