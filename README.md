# README #

### PORTFOLIO JAVA WEB APP  ###

Build a simple portfolio Java web app that displays the profile image, name, some text with
the experience and a 5 tweet list of the user’s Twitter timeline.
The second part should be a very simple API with 2 endpoints of the profile content.

### Requeriments ###

For building and running the application you need:

    * [JDK 11](https://www.oracle.com/java/technologies/javase-jdk11-downloads.html)
    * [Maven 3](https://maven.apache.org)
	
## Build the solution ##

This application was built like a approach a microservices using spring-boot. Moreover, the library twitter4j (http://twitter4j.org/en/) to conection to Twitter API and thymeleaf (+boostrap) to the frontend.

This solution has 3 differents modules that you have to run in order:

1. portfolio: this module is responsible for the conection to the data base, and expouse a Rest API to get an modify the profile information.

2. twitterclient: this module is responsible to consume the Twitter API and expouse a Rest API to get the timeline by user.

3. portfolioweb: this module consume the previous modules and present the information in a web page.

This solution uses principles from CQRS architecture(https://martinfowler.com/bliki/CQRS.html) to separate commands and queries.
Also, uses principles from Hexagonal architecture (https://dzone.com/articles/hexagonal-architecture-what-is-it-and-how-does-it).
	
## Running the application locally

There are several ways to run a Spring Boot application on your local machine. One way is to execute the `main` method to every module class from your IDE.
The modules that compouse this solution are:

1. portfolio the main class is co.com.zemoga.portfolio.PortfolioApplication.java run in the port 8080

2. twitterclient the main class is co.com.zemoga.twitterclient.TwitterclientApplication.java run in the port 8081

3. portfolioweb the main class is co.com.zemoga.portfolioweb.PortfoliowebApplication.java run in the port 8000

Alternatively you can use the [Spring Boot Maven plugin](https://docs.spring.io/spring-boot/docs/current/reference/html/build-tool-plugins-maven-plugin.html) like so:

```shell
mvn spring-boot:run
```
## Web Application ##
You can access the web application using this url in the navegator.
http://localhost:8000/portfolio/{user}

Example.
http://localhost:8000/portfolio/Daenerys

## Test Rest API ##

1. Rest API Portfolio.

Example to update a portfolio:

PUT http://localhost:8080/api/users

Body Json

{

	"id":101,
	
	"description":"Lord Commander of the Night's Watch and King of the Free Folk",
	
	"image":"https://pbs.twimg.com/profile_images/901947348699545601/hqRMHITj_400x400.jpg",
	
	"user":"LordSnow",
	
	"title":"Juan Nieve! 101"
	
}


Example to get portfolio by user

http://localhost:8080/api/users/{user}


GET http://localhost:8080/api/users/Daenerys

Response Json

{

	"id":2,
	
	"description":"The Mother of Dragons!!",
	
	"image":"https://pbs.twimg.com/profile_images/1117967801652617216/i8PWXebo_400x400.jpg",
	
	"user":"Daenerys",
	
	"title":"Daenerys Targaryen"
	
}


Example to get portfolio by id

http://localhost:8080/api/users/id/{userId}


Get http://localhost:8080/api/users/id/2

Response Json

{

	"id":2,
	
	"description":"The Mother of Dragons!!",
	
	"image":"https://pbs.twimg.com/profile_images/1117967801652617216/i8PWXebo_400x400.jpg",
	
	"user":"Daenerys",
	
	"title":"Daenerys Targaryen"
	
}


2. Rest API TwitterClient

Example to get timeline tweets by user

http://localhost:8081/tweets/{user}


GET http://localhost:8081/tweets/Daenerys

Response Json
[
	{
	
        "name": "Daenerys Targaryen",
		
        "text": "Get the popular Dragon Rings for FREE now!\uD83D\uDD25 Just cover shipping ✈️ \n\nLink in Bio\uD83D\uDE0D\n\nhttps://t.co/r26jDvtJQG https://t.co/HasBSpjo79",
		
        "profileImageURL": "http://pbs.twimg.com/profile_images/1117967801652617216/i8PWXebo_mini.jpg",
		
        "screenName": "Daenerys"
		
    }, {
	
        "name": "Daenerys Targaryen",
		
        "text": "RT @Daenerys: March 1st vs April 1st https://t.co/kebokQZTr7",
		
        "profileImageURL": "http://pbs.twimg.com/profile_images/1117967801652617216/i8PWXebo_mini.jpg",
		
        "screenName": "Daenerys"
		
    }, {
	
        "name": "Daenerys Targaryen",
		
        "text": "March 1st vs April 1st https://t.co/kebokQZTr7",
		
        "profileImageURL": "http://pbs.twimg.com/profile_images/1117967801652617216/i8PWXebo_mini.jpg",
		
        "screenName": "Daenerys"
		
    }
	
]


## Test ##
The test was built using JUnit, Mockito, MockMvc and MockRestServiceServer.

To run test you can use this command in every module:

```shell
mvn test
```

## Time ##
The totally time of dedication for resolve this test was 12 hours.



