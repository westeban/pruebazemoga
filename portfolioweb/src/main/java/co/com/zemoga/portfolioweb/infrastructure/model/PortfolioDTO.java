package co.com.zemoga.portfolioweb.infrastructure.model;

public class PortfolioDTO {

    private Integer id;
    private String description;
    private String image;
    private String user;
    private String title;

    public PortfolioDTO() {
    }

    public PortfolioDTO(Integer id, String description, String image, String user, String title) {
        this.id = id;
        this.description = description;
        this.image = image;
        this.user = user;
        this.title = title;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
