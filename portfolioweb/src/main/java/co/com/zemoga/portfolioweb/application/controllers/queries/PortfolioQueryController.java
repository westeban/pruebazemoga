package co.com.zemoga.portfolioweb.application.controllers.queries;

import co.com.zemoga.portfolioweb.infrastructure.model.PortfolioDTO;
import co.com.zemoga.portfolioweb.infrastructure.services.IPortfolioQueryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class PortfolioQueryController {

    @Autowired
    IPortfolioQueryService service;

    @RequestMapping(value = "/portfolio/{user}", method = RequestMethod.GET)
    public String showPortfolio(@PathVariable String user, Model model) {
        PortfolioDTO portfolioDTO = service.getPortfolio(user);
        model.addAttribute("portfolio", portfolioDTO);
        model.addAttribute("tweets", service.getTimeLineUser(portfolioDTO.getUser()));
        return "index";
    }

}
