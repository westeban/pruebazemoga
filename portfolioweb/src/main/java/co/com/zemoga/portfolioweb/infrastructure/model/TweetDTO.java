package co.com.zemoga.portfolioweb.infrastructure.model;

public class TweetDTO {

    private String name;
    private String ScreenName;
    private String text;
    private String profileImageURL;


    public TweetDTO() {
    }

    public TweetDTO(String name, String screenName, String text, String profileImageURL) {
        this.name = name;
        ScreenName = screenName;
        this.text = text;
        this.profileImageURL = profileImageURL;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getScreenName() {
        return ScreenName;
    }

    public void setScreenName(String screenName) {
        ScreenName = screenName;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getProfileImageURL() {
        return profileImageURL;
    }

    public void setProfileImageURL(String profileImageURL) {
        this.profileImageURL = profileImageURL;
    }
}