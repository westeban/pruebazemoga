package co.com.zemoga.portfolioweb.infrastructure.services.queries;

import co.com.zemoga.portfolioweb.infrastructure.model.PortfolioDTO;
import co.com.zemoga.portfolioweb.infrastructure.model.TweetDTO;
import co.com.zemoga.portfolioweb.infrastructure.services.IPortfolioQueryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PortfolioQueryService implements IPortfolioQueryService {

    @Autowired
    private RestTemplate restTemplate;

    @Override
    public PortfolioDTO getPortfolio(String user) {
        try {
            return restTemplate.getForObject("http://localhost:8080/api/users/" + user, PortfolioDTO.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new PortfolioDTO();
    }

    @Override
    public List<TweetDTO> getTimeLineUser(String user){
        TweetDTO[] tweets = {};
        try {
            tweets = restTemplate.getForEntity("http://localhost:8081/tweets/"+user, TweetDTO[].class).getBody();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Arrays.asList(tweets).stream().limit(5L).collect(Collectors.toList());
    }
}
