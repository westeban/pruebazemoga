package co.com.zemoga.portfolioweb.infrastructure.services;

import co.com.zemoga.portfolioweb.infrastructure.model.PortfolioDTO;
import co.com.zemoga.portfolioweb.infrastructure.model.TweetDTO;

import java.util.List;

public interface IPortfolioQueryService {

    PortfolioDTO getPortfolio(String user);

    List<TweetDTO> getTimeLineUser(String user);


}
