package co.com.zemoga.portfolioweb.infrastructure.services.queries;

import co.com.zemoga.portfolioweb.SpringTestConfig;
import co.com.zemoga.portfolioweb.infrastructure.model.PortfolioDTO;
import co.com.zemoga.portfolioweb.infrastructure.model.TweetDTO;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.ExpectedCount;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.fail;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = SpringTestConfig.class)
public class PortfolioQueryServiceTest {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private PortfolioQueryService service;

    private MockRestServiceServer mockServer;
    private ObjectMapper mapper = new ObjectMapper();

    @Before
    public void init() {
        mockServer = MockRestServiceServer.createServer(restTemplate);
    }

    @Test
    public void givenUser_whenGetTimeLineUser_thenReturnListTweetDTO() {
        String user = "user";
        TweetDTO tweetDTO = new TweetDTO("name", "screenName", "text", "profileImageURL");
        List<TweetDTO> tweets = new ArrayList<>();
        tweets.add(tweetDTO);
        try {
            mockServer.expect(ExpectedCount.once(),
                    requestTo(new URI("http://localhost:8081/tweets/" + user)))
                    .andExpect(method(HttpMethod.GET))
                    .andRespond(withStatus(HttpStatus.OK)
                            .contentType(MediaType.APPLICATION_JSON)
                            .body(mapper.writeValueAsString(tweets))
                    );

            List<TweetDTO> tweetsResult = service.getTimeLineUser(user);

            mockServer.verify();
            assertThat(tweetsResult.size()).isEqualTo(tweets.size());
            assertThat(tweetsResult.get(0)).isEqualToComparingFieldByField(tweets.get(0));
        } catch (URISyntaxException e) {
            e.printStackTrace();
            fail();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            fail();
        }

    }

    @Test
    public void givenUser_whenGetPortfolio_thenReturnPortfolioDTO() {
        String user = "user";
        PortfolioDTO portfolioDTO = new PortfolioDTO(102, "description", "image", user, "title");

        try {
            mockServer.expect(ExpectedCount.once(),
                    requestTo(new URI("http://localhost:8080/api/users/"+user)))
                    .andExpect(method(HttpMethod.GET))
                    .andRespond(withStatus(HttpStatus.OK)
                            .contentType(MediaType.APPLICATION_JSON)
                            .body(mapper.writeValueAsString(portfolioDTO))
                    );

            PortfolioDTO portfolioResult = service.getPortfolio(user);

            mockServer.verify();
            assertThat(portfolioDTO).isEqualToComparingFieldByField(portfolioResult);
        } catch (URISyntaxException e) {
            e.printStackTrace();
            fail();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            fail();
        }

    }
}
