package co.com.zemoga.portfolioweb.application.controllers.queries;

import co.com.zemoga.portfolioweb.infrastructure.model.PortfolioDTO;
import co.com.zemoga.portfolioweb.infrastructure.model.TweetDTO;
import co.com.zemoga.portfolioweb.infrastructure.services.IPortfolioQueryService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.fail;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(PortfolioQueryController.class)
public class PortfolioQueryControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private IPortfolioQueryService service;

    @Test
    public void givenUserId_whenGetPortfolio_thenReturnPortfolio() {
        String user = "user";
        PortfolioDTO portfolio = new PortfolioDTO(102, "description", "image", user, "title");
        TweetDTO tweetDTO = new TweetDTO("name", "screenName", "text", "profileImageURL");
        List<TweetDTO> tweets = new ArrayList<>();
        tweets.add(tweetDTO);

        given(service.getPortfolio(user)).willReturn(portfolio);
        given(service.getTimeLineUser(user)).willReturn(tweets);

        try {
            mvc.perform(get("/portfolio/"+user)
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andExpect(model().attribute("portfolio", is(portfolio)))
                    .andExpect(model().attribute("tweets", is(tweets)));
            verify(service, times(1)).getPortfolio(user);
            verify(service, times(1)).getTimeLineUser(user);
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }

}