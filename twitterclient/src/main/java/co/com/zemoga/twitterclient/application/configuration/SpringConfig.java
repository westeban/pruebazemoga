package co.com.zemoga.twitterclient.application.configuration;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import twitter4j.Twitter;
import twitter4j.TwitterFactory;

@Configuration
@EnableAutoConfiguration
@ComponentScan("co.com.zemoga.twitterclient.application.configuration")
public class SpringConfig {

    @Bean
    public Twitter twitter() {
        return TwitterFactory.getSingleton();
    }

}