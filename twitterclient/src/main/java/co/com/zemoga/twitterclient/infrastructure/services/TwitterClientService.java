package co.com.zemoga.twitterclient.infrastructure.services;

import co.com.zemoga.twitterclient.infrastructure.model.Tweet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TwitterClientService implements ITwitterClientService {

    @Autowired
    Twitter twitter;

    public List<Tweet> getTimeLine(String user) {
        List<Status> statuses = new ArrayList<>();
        try{
         statuses = twitter.getUserTimeline(user);
        } catch (TwitterException te) {
            te.printStackTrace();
        }

        return statuses.stream().map(
                item -> new Tweet(item.getUser().getName(), item.getUser().getScreenName(), item.getText(), item.getUser().getMiniProfileImageURL())).collect(
                Collectors.toList());
    }



}
