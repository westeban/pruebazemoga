package co.com.zemoga.twitterclient.application.controlers;

import co.com.zemoga.twitterclient.infrastructure.model.Tweet;
import co.com.zemoga.twitterclient.infrastructure.services.ITwitterClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class TwitterQueryRestController {

    @Autowired
    ITwitterClientService iTwitterClientService;

    @GetMapping(value = "/tweets/{user}")
    public List<Tweet> getTweets(@PathVariable String user) {
        return iTwitterClientService.getTimeLine(user);
    }

}
