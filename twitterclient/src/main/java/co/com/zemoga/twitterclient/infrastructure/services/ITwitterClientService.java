package co.com.zemoga.twitterclient.infrastructure.services;

import co.com.zemoga.twitterclient.infrastructure.model.Tweet;

import java.util.List;

public interface ITwitterClientService {

    List<Tweet> getTimeLine(String user);
}
