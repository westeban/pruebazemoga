package co.com.zemoga.twitterclient.application.controllers;

import co.com.zemoga.twitterclient.TwitterclientApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.fail;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = TwitterclientApplication.class)
@AutoConfigureMockMvc
public class TwitterQueryRestControllerIntegrationTest {

    @Autowired
    private MockMvc mvc;

    @Test
    public void givenUser_whenGetTweets_thenReturnListTweets() {
        String user= "LordSnow";

        try {
            mvc.perform(get("/tweets/"+user)
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$", hasSize(greaterThanOrEqualTo(1))))
                    .andExpect(jsonPath("$[0].screenName", is(user)));
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }
}
