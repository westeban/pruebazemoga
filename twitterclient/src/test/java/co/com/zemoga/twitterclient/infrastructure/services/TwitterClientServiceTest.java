package co.com.zemoga.twitterclient.infrastructure.services;

import co.com.zemoga.twitterclient.SpringTestConfig;
import co.com.zemoga.twitterclient.infrastructure.model.Tweet;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import twitter4j.Twitter;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = SpringTestConfig.class)
public class TwitterClientServiceTest {

    @Autowired
    Twitter twitter;

    @Autowired
    TwitterClientService service;

    @Test
    public void givenUser_whenGetTimeLine_thenReturnListOfTweetEmpty() {
        String user = "user";

        List<Tweet> tweetList = service.getTimeLine(user);

        assertThat(tweetList.isEmpty()).isTrue();
    }

    @Test
    public void givenUser_whenGetTimeLine_thenReturnListOfTweet() {
        String user = "LordSnow";

        List<Tweet> tweetList = service.getTimeLine(user);

        assertThat(tweetList.isEmpty()).isFalse();
    }
}
