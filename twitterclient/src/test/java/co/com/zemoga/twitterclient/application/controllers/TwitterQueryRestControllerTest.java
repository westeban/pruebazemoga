package co.com.zemoga.twitterclient.application.controllers;

import co.com.zemoga.twitterclient.application.controlers.TwitterQueryRestController;
import co.com.zemoga.twitterclient.infrastructure.model.Tweet;
import co.com.zemoga.twitterclient.infrastructure.services.ITwitterClientService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.fail;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.only;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(TwitterQueryRestController.class)
public class TwitterQueryRestControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private ITwitterClientService service;

    @Test
    public void givenUser_whenGetTweets_thenReturnListTweets() {
        String user= "user";
        Tweet tweet = new Tweet("name", "screenName", "text", "profileImageURL");
        List<Tweet> tweets = new ArrayList<>();
        tweets.add(tweet);
        given(service.getTimeLine(user)).willReturn(tweets);

        try {
            mvc.perform(get("/tweets/"+user)
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$[0].name", is(tweet.getName())))
                    .andExpect(jsonPath("$[0].screenName", is(tweet.getScreenName())))
                    .andExpect(jsonPath("$[0].text", is(tweet.getText())))
                    .andExpect(jsonPath("$[0].profileImageURL", is(tweet.getProfileImageURL())));
            verify(service, only()).getTimeLine(user);
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }
}
